import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите строку состояющую из слов разделенных пробелами ");
        String str = sc.nextLine();
        String[] strings = str.split(" ");
        HashMap<String, Integer> hm = new HashMap<>();
        int r = 1;
        for (int i = 0; i < strings.length; i++) {

            if (hm.containsKey(strings[i])) {
                hm.put(strings[i], hm.get(strings[i] ) +1 );

            } else {
                hm.put(strings[i], 1);
            }

        }
        int val = 1;
        hm.entrySet().forEach(entry -> {

            if (entry.getValue() == strings.length){
                System.out.println(entry.getKey() +  "встречается максимальное количество раз");

            }
            // if (val) {
        });
        System.out.println(Arrays.asList(hm));
        Map.Entry<String, Integer> maxEntry =
                Collections.max(hm.entrySet(), Map.Entry.comparingByValue());
        System.out.println(maxEntry);
    }
}