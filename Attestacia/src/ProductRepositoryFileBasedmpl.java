import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ProductRepositoryFileBasedmpl implements ProductRepository {


    @Override
    public Product findById(Integer id) {
        Product product = new Product();
        try {
            List<String> lines = Files.readAllLines(Paths.get("Attestacia\\src\\file.txt").toAbsolutePath(), UTF_8);
            for (String line : lines) {
                String[] words = line.split("\\|"); //разбили на массив

                Integer idInLine = Integer.valueOf(words[0]); //присвоение первого элемента из строчки файла
                if (idInLine.equals(id)) {
                    product.setId(idInLine);
                    product.setName(words[1]);
                    product.setPrice(Double.parseDouble(words[2]));
                    product.setCount(Integer.parseInt(words[3]));
                }
            }
            return product;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        List<Product> products = new ArrayList<>();
        Product product = new Product();
        try {
            List<String> lines = Files.readAllLines(Paths.get("Attestacia\\src\\file.txt").toAbsolutePath(), UTF_8);
            for (String line : lines) {
                String[] words = line.split("\\|"); //разбили на массив

                String name = words[1]; //присвоение первого элемента из строчки файла
                if (name.contains(title)) {
                    product.setId(Integer.parseInt(words[0]));
                    product.setName(name);
                    product.setPrice(Double.parseDouble(words[2]));
                    product.setCount(Integer.parseInt(words[3]));
                    products.add(product);
                }

            }
            return products;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void update(Product product) {

    }
}
