public class Ellipse extends Figure {
    public void getPerimetr(double smallOs, double bigOs) {

        double ellipse = 4 * (Math.PI * smallOs * bigOs + (smallOs - bigOs) * (smallOs - bigOs)) / (smallOs + bigOs);
        System.out.println("Периметр еллипса " + ellipse);

    }

    public void getPloshad(double smallOs, double bigOs) {
        double area = Math.PI * (smallOs * bigOs);
        System.out.println("Площадь эллипса равна: " + area);
    }
}