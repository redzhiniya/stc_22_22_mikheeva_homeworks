import java.util.Scanner;

public class Rectangle extends Figure {


    public  void getPerimetr(int a, int b) {
        int p = a * 2 + b * 2;

        System.out.println("P = " + p + " см");
    }

    public  void getPloshad(int a, int b) {

        int s = a * b;
        System.out.println("S = " + s + " см^2");

    }
}
