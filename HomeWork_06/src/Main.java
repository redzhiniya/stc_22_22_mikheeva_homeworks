public class Main {
    public static void main(String[] args){
        Circle circle = new Circle();
        circle.getPerimetr(2.0);
        circle.getPloshad(2.0);
        circle.move(2,3);

        Ellipse ellipse = new Ellipse();
        ellipse.getPerimetr(1.0,2.0);
        ellipse.getPloshad(1.0,2.0);
        ellipse.move(1, 2);

        Rectangle rectangle = new Rectangle();
        rectangle.getPerimetr(1, 2);
        rectangle.getPloshad(1,2);

        Square square = new Square();
        square.getPerimetr(1,2);
        square.getPloshad(1,2);
    }

}
