public class Circle extends Figure {
    public  void getPerimetr(double radius) {
        double circumference= Math.PI * 2*radius;
        System.out.println("Длина окружности равна: " + circumference) ;
    }
    public  void getPloshad(double radius){
        double area = Math.PI * (radius * radius);
        System.out.println("Площадь круга равна: " + area);
    }
}
