package ru.inno.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.inno.ec.models.Order;
import ru.inno.ec.security.details.CustomUserDetails;
import ru.inno.ec.services.OrdersService;

import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/orders")
public class OrdersController {

    private final OrdersService ordersService;

    @GetMapping
    public String getOrdersPage(@AuthenticationPrincipal CustomUserDetails customUserDetails, Model model) {
        model.addAttribute("role", customUserDetails.getUser().getRole());
        model.addAttribute("order",
                List.of(
                        Order.builder().id(1L).title("Java").build(),
                        Order.builder().id(2L).title("SQL").build(),
                        Order.builder().id(3L).title("Spring").build()
                ));
        return "order/order_page";
    }


    @PostMapping("/{order-id}/students")
    public String addStudentToOrder(@PathVariable("order-id") Long orderId,
                                     @RequestParam("student-id") Long studentId) {
        ordersService.addStudentToOrder(orderId, studentId);
        return "redirect:/orders/" + orderId;
    }

    @GetMapping("/{order-id}")
    public String getOrderPage(@PathVariable("order-id") Long orderId, Model model) {
        //LazyInitializationBeanFactoryPostProcessor ordersService = Id;
        model.addAttribute("order", ordersService.getOrder(orderId));
        model.addAttribute("notInOrderStudents", ordersService.getNotInOrderStudents(orderId));
        model.addAttribute("inOrderStudents", ordersService.getInOrderStudents(orderId));
        return "order/order_page";
    }
}
