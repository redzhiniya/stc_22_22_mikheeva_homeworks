package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Order;
import ru.inno.ec.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByOrdersNotContainsAndState(Order order, User.State state);

    List<User> findAllByOrdersContains(Order order);

    Optional<User> findByEmail(String email);
}
