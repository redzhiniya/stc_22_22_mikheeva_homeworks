package ru.inno.ec.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.inno.ec.models.Order;

public interface OrdersRepository extends JpaRepository<Order, Long> {
}
