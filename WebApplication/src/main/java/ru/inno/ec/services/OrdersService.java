package ru.inno.ec.services;

import ru.inno.ec.models.Order;
import ru.inno.ec.models.User;

import java.util.List;

public interface OrdersService {
    void addStudentToOrder(Long orderId, Long studentId);

    Order getOrder(Long orderId);

    List<User> getNotInOrderStudents(Long orderId);

    List<User> getInOrderStudents(Long orderId);
}
