package ru.inno.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.models.Order;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.OrdersRepository;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.services.OrdersService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class OrdersServiceImpl implements OrdersService {

    private final OrdersRepository ordersRepository;
    private final UsersRepository usersRepository;

    @Override
    public void addStudentToOrder(Long orderId, Long studentId) {
        Order order = ordersRepository.findById(orderId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();

        student.getOrders().add(order);

        usersRepository.save(student);
    }

    @Override
    public Order getOrder(Long orderId) {
        return ordersRepository.findById(orderId).orElseThrow();
    }

    @Override
    public List<User> getNotInOrderStudents(Long orderId) {
        Order order = ordersRepository.findById(orderId).orElseThrow();
        return usersRepository.findAllByOrdersNotContainsAndState(order, User.State.CONFIRMED);
    }

    @Override
    public List<User> getInOrderStudents(Long orderId) {
        Order order = ordersRepository.findById(orderId).orElseThrow();
        return usersRepository.findAllByOrdersContains(order);
    }
}
