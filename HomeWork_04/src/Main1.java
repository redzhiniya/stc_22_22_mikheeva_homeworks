import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main1 {
    public static void sumInterval() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите левую границу интервала: ");
        int left = sc.nextInt();
        System.out.print("Введите правую границу интервала: ");
        int right = sc.nextInt();
        if (left > right) {
            System.out.println("-1");
        } else {
            System.out.println(left + right);
        }
    }

    public static boolean isEven(int number) {
        return number % 2 == 0;
    }

    public static void evenArray() {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите количество элементов в массиве: ");
        n = sc.nextInt();
        int[] array = new int[n];
        System.out.print("Введите элементы в массиве: ");
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }
        for (int i = 0; i < array.length; i++) {
            if (isEven(array[i])) {
                System.out.println(array[i]);
            }
        }
    }

    public static void main(String[] args) {
        sumInterval();
        evenArray();
    }
}