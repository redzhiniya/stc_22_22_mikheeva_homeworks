public class OddNumbersPrintTask extends AbstractNumbersPrintTask{
    @Override
    public void complete() {
        int from = super.getFrom();
        int to = super.getTo();
        for (int i = from; i <= to; i++) {
            if (i % 2 != 0){
                System.out.println(i);
            }
        }
    }
}