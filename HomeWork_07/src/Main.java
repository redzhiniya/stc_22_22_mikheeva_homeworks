public class Main {
    public static void comleteAllTasks(Task[] tasks) {
        for (int i = 0; i < tasks.length; i++) {
            tasks[i].complete();
        }
    }
    public static void main(String[] args){
        EvenNumbersPrintTask n1 = new EvenNumbersPrintTask();
        n1.setFrom(1);
        n1.setTo(10);
        OddNumbersPrintTask n2 = new OddNumbersPrintTask ();
        n2.setFrom(13);
        n2.setTo(17);
        Task[] tasks1 = {(Task) n1, (Task) n2};
        comleteAllTasks(tasks1);
    }
}
