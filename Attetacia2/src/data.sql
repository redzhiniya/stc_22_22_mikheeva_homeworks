insert into voditel (first_name, last_name, phoneNumber, experience, age, drivers_licence, license_B, rating)
values ('Регина', 'Михеева', '89999999999', 7, 27, true, true, 4);
insert into voditel (first_name, last_name, phoneNumber, experience, age, drivers_licence, license_B, rating)
values ('Жанна', 'Толуева', '89999999998', 6, 27, false, true, 4);
insert into voditel (first_name, last_name, phoneNumber, experience, age, drivers_licence, license_B, rating)
values ('Кирилл', 'Яколвев', '89999999997', 6, 29, false, true, 5);
insert into voditel (first_name, last_name, phoneNumber, experience, age, drivers_licence, license_B, rating)
values ('Денис', 'Буй', '89999999996', 6, 29, false, true, 5);
insert into voditel (first_name, last_name, phoneNumber, experience, age, drivers_licence, license_B, rating)
values ('Анна', 'Голубева', '89999999995', 6, 30, false, false, 0);

insert into car (model, color, number, id_voditel)
values ('Ford', 'white', '149', '1');
insert into car (model, color, number, id_voditel)
values ('Kia', 'black', '330','1');
insert into car (model, color, number, id_voditel)
values ('Kia', 'white', '159', '1');
insert into car (model, color, number, id_voditel)
values ('Subaru', 'red', '112', '1');
insert into car (model, color, number, id_voditel)
values ('Kia', 'white', '117', '1');

insert into trip (vodiel_id, car_id, date_trip, time_trip_minute)
values ('1', '1', '26.11.2022', '15');
insert into trip (vodiel_id, car_id, date_trip, time_trip_minute)
values ('2', '2', '26.11.2022', '65');
insert into trip (vodiel_id, car_id, date_trip, time_trip_minute)
values ('1', '2', '26.11.2022', '30');
insert into trip (vodiel_id, car_id, date_trip, time_trip_minute)
values ('2', '3', '26.11.2022', '10');
insert into trip (vodiel_id, car_id, date_trip, time_trip_minute)
values ('1', '4', '26.11.2022', '16');