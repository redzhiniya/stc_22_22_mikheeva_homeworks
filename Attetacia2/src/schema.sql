create table voditel
(
    id     serial primary key,
    first_name      char(20) default 'DEFAULT_FIRSTNAME',
    last_name       char(20) default 'DEFAULT_LASTNAME',
    phoneNumber     char(20),
    experience      integer,
    age             integer,
    drivers_licence bool,
    license_B       bool,
    rating          integer check ( rating >= 0 and rating <= 5)

);
create table Car
(
    id     serial primary key,
    model  char(15),
    color  char(20),
    number char(10),
    id_voditel int,
    foreign key (id_voditel) references voditel(id)

);
create table trip(
                     id     serial primary key,
                     vodiel_id int,
                     car_id int,
                     date_trip char(22),
                     time_trip_minute char (10),
                     foreign key (vodiel_id) references voditel(id),
                     foreign key (car_id) references car(id)

);