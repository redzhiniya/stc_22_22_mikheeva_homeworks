import java.util.List;

public interface Task {
    void complete();
}
